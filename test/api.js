var mocha = require('mocha')
var chai = require('chai')
var  chaihttp = require('chai-http')
var server = require('../server')

var should= chai.should()

chai.use(chaihttp) // Configurar chau con el modulo http

//==================================
// Definir conjunto de pruebas
//==================================

describe('Test de comectividad', () => {

  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx').get ('/').end((err, res) => {
          //console.log(res)
          res.should.have.status(200) // Propiedad status con valor 200
          done()
        })

      })
  })

  describe('Test de API', () => {
    it('Raìz de la API contesta', (done) => {
      chai.request('http://localhost:3000').get ('/v3').end((err, res) => {
            //console.log(res)
            res.should.have.status(200) // Propiedad status con valor 200
            done()
          })

        })

        it('Raìz de la API funciona', (done) => {
          chai.request('http://localhost:3000').get ('/v3').end((err, res) => {
                //console.log(res)
                res.should.have.status(200) // Propiedad status con valor 200
                res.body.should.be.a('array')
                done()
              })

            })

      it('Raìz de la API, devuelve dos colecciones', (done) => {
        chai.request('http://localhost:3000').get ('/v3').end((err, res) => {
              //console.log(res.body)
              res.should.have.status(200) // Propiedad status con valor 200
              res.body.should.be.a('array')
              res.body.length.should.be.eql(2)
              done()
          })
      })
      it('Raìz de la API, devuelve los objetos correctos', (done) => {
        chai.request('http://localhost:3000').get ('/v3').end((err, res) => {
              console.log(res.body)
              res.should.have.status(200) // Propiedad status con valor 200
              res.body.should.be.a('array')
              res.body.length.should.be.eql(2)
              for (var i = 0; i < res.body.length; i++) {
                res.body[i].should.have.property('recurso')
                res.body[i].should.have.property('url')
              }
              done()
            })
       })


       describe('Test de API movimientos', () => {
         var idMovs = 'W00002'
         it('Raìz de la API movimientos contesta', (done) => {
           chai.request('http://localhost:3000').get ('/v3/movimientos').end((err, res) => {
                 console.log("Total : " + res.body.length)
                 res.should.have.status(200) // Propiedad status con valor 200
                 res.body.should.be.a('array').length
                 for (var i = 0; i < 1; i++) {
                   console.log(res.body[2].should.have)
                   //res.body[i].should.have.property('url')
                 }
                 done()
               })
             })
         })

    })
